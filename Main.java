package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        String kelompok, nama, alamat;
        int jumlah, noid, nohp;
        Scanner s = new Scanner(System.in);
        System.out.println("PROGRAM PENDAFTARAN SIMAKSI GUNUNG");
        System.out.println("MASUKKAN DATA DIRI PENDAKI");
        System.out.print("Masukkan Nama Kelompok : ");
        kelompok = s.nextLine();
        System.out.print("Masukkan Jumlah Anggota : ");
        jumlah = s.nextInt();
        for (int i = 0; i < jumlah; i++) {
            System.out.print("Masukkan Nama anggota ke-" + (i + 1) + " : ");
            nama = s.next();
            System.out.print("Masukkan NIK anggota ke-" + (i + 1) + " : ");
            noid = s.nextInt();
            System.out.print("Masukkan No Telp anggota ke-" + (i + 1) + " : ");
            nohp = s.nextInt();
        }

        int tenda, sb, matras, masak, senter, jashujan, jaket;

        System.out.println("MASUKKAN DATA PERALATAN");
        System.out.print("Masukkan Jumlah Tenda : ");
        tenda = s.nextInt();
        System.out.print("Masukkan Jumlah Sleeping Bag : ");
        sb = s.nextInt();
        System.out.print("Masukkan Jumlah Matras : ");
        matras = s.nextInt();
        System.out.print("Masukkan Jumlah Set Peralatan Masak : ");
        masak = s.nextInt();
        System.out.print("Masukkan Jumlah Senter : ");
        senter = s.nextInt();
        System.out.print("Masukkan Jumlah Jaket : ");
        jaket = s.nextInt();
        if(tenda<(jumlah/4)){
            System.out.println("Jumlah Tenda kurang mohon dilengkapi terlebih dahulu");
        }else if(sb<jumlah){
            System.out.print("Jumlah Sleeping Bag kurang mohon dilengkapi terlebih dahulu");
        }else if(matras<(tenda*3)){
            System.out.println("Jumlah Matras Kurang mohon dilengkapi terlebih dahulu");
        }else if(masak<(jumlah/4)){
            System.out.println("Jumlah Set Peralatan Masak kurang mohon dilengkapi terlebih dahulu");
        }else if(senter<jumlah){
            System.out.println("Jumlah Senter kurang mohon dilengkapi terlebih dahulu");
        }else if(jaket<jumlah){
            System.out.println("Jumlah Jaket kurang mohon dilengkapi terlebih dahulu");
        }else{
            System.out.println("PENDAFTARAN SIMAKSI SUKSES SILAHKAN TRANSFER SEJUMLAH Rp."+20000*jumlah+" KE NO REK 18000182951 DAN DIBAWA KE BASECAMP SEBAGAI BUKTI PENDAKIAN YANG SAH");
        }
    }
}
